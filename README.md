# CFTR Lifecylce Map

This is the project page for the CFTR Lifecylce map. It includes current versions of the SBGN Models, images of disease maps in PDF format and supplementary materials such as gene lists, literature overviews etc.

For further information and collaboration, please view our [publication](https://doi.org/10.3390/ijms22147590)
and [email](liza.vinhoven@med.uni-goettingen.de) us.

Citation: \
Vinhoven, L.; Stanke, F.; Hafkemeyer, S.; Nietert, M.M. CFTR Lifecycle Map—A Systems Medicine Model of CFTR Maturation to Predict Possible Active Compound Combinations. Int. J. Mol. Sci. 2021, 22, 7590. https://doi.org/10.3390/ijms22147590 

